<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Sessionscholaire;
use App\Models\Classe;


class StoreClasse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|unique:classes,code'.($this->id ? ",$this->id" : ''),
            'nom'=>'required',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!Classe::find($this->id)) {
                $validator->errors()->add('id', 'Id invalide');
            }
            //TODO: checker si id existe
            if (!Sessionscholaire::find($this->sessionscholaire['id'])) {
                $validator->errors()->add('sessionscholaireid', 'Session scholaire invalide');
            }

        });
    }
}
