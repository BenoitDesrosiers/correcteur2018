<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use View;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->ability('','enseigner')) {
            return  View::make('homePageProf');
        } else {
            return View::make('homePageEtudiant');
        }
    }
}
