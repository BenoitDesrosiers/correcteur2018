<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::group(['middleware'=>['role:admin|professeur']], function() {
Route::group(['middleware'=>['auth']], function() {

    //routes pour Classes
    Route::get('classes/indexApi', ['as' => 'classes.indexApi', 'uses' => 'ClassesController@indexApi']);
    Route::get('classes/editApi/{id}', ['as' => 'classes.editApi', 'uses' => 'ClassesController@editApi']);
    Route::post('classes/updateApi/{id}', ['as' => 'classes.updateApi', 'uses' => 'ClassesController@updateApi']);
    Route::get('classes/deleteApi/{id}', ['as' => 'classes.deleteApi', 'uses' => 'ClassesController@deleteApi']);

    //routes pour Sessions
    Route::get('sessions/liste', ['as' => 'session.listeApi', 'uses' => 'SessionsController@listeApi']);


});